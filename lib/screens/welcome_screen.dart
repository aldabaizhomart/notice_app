import 'package:flutter/material.dart';
import 'package:notice_app/screens/question_screen.dart';
import 'package:notice_app/widgets/text_widget.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  WelcomeScreenState createState() => WelcomeScreenState();
}

class WelcomeScreenState extends State<WelcomeScreen> {
  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;

  List<Map<String, String>> walkthroughPages = [
    {
      'image': 'assets/img/welcome1.png',
      'title': 'Track nav_content',
      'description':
          'Track and manage all your subscriptions easily from a single place'
    },
    {
      'image': 'assets/img/welcome2.png',
      'title': 'Reminders',
      'description':
          'Schedule notifications for your subscriptions renewal at any given time'
    },
    {
      'image': 'assets/img/welcome3.png',
      'title': 'Statistics',
      'description':
          'Always care of your spending with the right data and insightful charts'
    },
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: const Color.fromRGBO(35, 35, 35, 1),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            children: [
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => const QuestionScreen()));
                    },
                    child: const AppText(
                        text: 'Skip',
                        color: Colors.grey,
                        fontSize: 14,
                        fontWeight: FontWeight.w500),
                  )
                ],
              ),
              Expanded(
                child: PageView.builder(
                  controller: _pageController,
                  itemCount: walkthroughPages.length,
                  onPageChanged: (int page) {
                    setState(() => _currentPage = page);
                  },
                  itemBuilder: (_, index) {
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image(
                          image: AssetImage(walkthroughPages[index]['image']!),
                          fit: BoxFit.cover,
                        ),
                        AppText(
                          text: walkthroughPages[index]['title']!,
                          color: Colors.white,
                          fontSize: 32,
                          fontWeight: FontWeight.w600,
                        ),
                        AppText(
                          text: walkthroughPages[index]['description']!,
                          color: Colors.white,
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          textAlign: TextAlign.center,
                        ),
                      ],
                    );
                  },
                ),
              ),
              SmoothPageIndicator(
                controller: _pageController,
                count: walkthroughPages.length,
                effect: const WormEffect(
                    dotHeight: 10,
                    dotWidth: 10,
                    dotColor: Colors.grey,
                    activeDotColor: Colors.blue),
              ),
              const SizedBox(height: 20),
              SizedBox(
                width: double.maxFinite,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    foregroundColor: Colors.indigo,
                    backgroundColor: Colors.cyan,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50),
                    ),
                  ),
                  onPressed: () {
                    if (_currentPage == walkthroughPages.length - 1) {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => const QuestionScreen()));
                    } else {
                      _pageController.nextPage(
                          duration: const Duration(milliseconds: 400),
                          curve: Curves.easeInOut);
                    }
                  },
                  child: const AppText(
                    text: 'Get Started',
                    color: Colors.white,
                    fontSize: 16,
                  ),
                ),
              ),
              const SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }
}
