import 'dart:async';
import 'dart:math';

import 'package:capped_progress_indicator/capped_progress_indicator.dart';
import 'package:flutter/material.dart';
import 'package:notice_app/screens/subscription_screen.dart';
import 'package:notice_app/widgets/text_widget.dart';

class PersonificationScreen extends StatefulWidget {
  @override
  _PersonificationScreenState createState() => _PersonificationScreenState();
}

class _PersonificationScreenState extends State<PersonificationScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  Timer? _textTimer;
  Timer? _buttonTimer;
  int _currentPage = 0;
  bool _showButton = false;

  final List<String> _texts = [
    "Curating your ideal interface",
    "Molding the app to your taste",
    "Shaping every detail to fit"
  ];

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 3), // Duration for a full rotation
      vsync: this,
    );
    _controller.repeat();

    // Initialize timer to change text every 2 seconds
    _textTimer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (_currentPage < _texts.length) {
        setState(() {
          _currentPage++; // Increment to show next text
        });
      } else {
        _textTimer?.cancel(); // Stop the timer after fourth text is shown
      }
    });

    // Initialize timer to show button after 5 seconds
    _buttonTimer = Timer(const Duration(seconds: 5), () {
      setState(() {
        _showButton = true; // Show the button
      });
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    _textTimer?.cancel();
    _buttonTimer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(35, 35, 35, 1),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                const SizedBox(
                  height: 50,
                ),
                const Center(
                  child: AppText(
                    text: 'Personafication',
                    color: Colors.white,
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(
                  height: 35,
                ),
                Center(
                  child: AnimatedBuilder(
                    animation: _controller,
                    child: Stack(
                      children: [
                        SizedBox(
                          height: 150,
                          width: 150,
                          child: CircularCappedProgressIndicator(
                            color: Colors.blue.withOpacity(0.2),
                            strokeWidth: 25,
                            value: 1,
                            strokeCap: StrokeCap.round,
                          ),
                        ),
                        const SizedBox(
                          height: 150,
                          width: 150,
                          child: CircularCappedProgressIndicator(
                            strokeWidth: 25,
                            value: 0.25,
                            strokeCap: StrokeCap.round,
                          ),
                        ),
                      ],
                    ),
                    builder: (context, child) {
                      return Transform.rotate(
                        angle:
                            _controller.value * 2.0 * pi, // Convert to radians
                        child: child,
                      );
                    },
                  ),
                ),
                const SizedBox(
                  height: 35,
                ),
                const AppText(
                  text:
                      'Please wait\nNow we will configure the app\nbased on your answers',
                  textAlign: TextAlign.center,
                  fontSize: 16,
                ),
                const SizedBox(height: 35),
                // Display texts sequentially
                for (int i = 0; i < _currentPage; i++)
                  Padding(
                    padding: const EdgeInsets.only(bottom: 10.0),
                    child: Text(
                      _texts[i],
                      style: const TextStyle(
                        color: Color.fromRGBO(0, 122, 255, 1),
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
              ],
            ),
            if (_showButton) // Show button after 5 seconds
              SizedBox(
                height: 40,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.blue,
                    minimumSize: const Size(double.infinity, 50),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12),
                    ),
                  ),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => const SubscriptionScreen()));
                  },
                  child: const AppText(
                    text: 'Continue',
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                    fontSize: 16,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
