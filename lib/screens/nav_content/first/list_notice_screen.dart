import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:notice_app/screens/nav_content/first/add_notice_screen.dart';
import 'package:notice_app/widgets/blue_tag1.dart';
import 'package:notice_app/widgets/blue_tag2.dart';
import 'package:notice_app/widgets/grey_tag.dart';
import 'package:notice_app/widgets/text_widget.dart';

class ListNoticeScreen extends StatefulWidget {
  final List<Map<String, String>> records;

  const ListNoticeScreen({super.key, required this.records});

  @override
  State<ListNoticeScreen> createState() => _ListNoticeScreenState();
}

class _ListNoticeScreenState extends State<ListNoticeScreen> {
  bool isActiveSelected = true;

  String formatDateString(String dateString) {
    try {
      final DateTime date = DateFormat('d/M/yyyy').parse(dateString);
      return DateFormat('d MMMM, yyyy').format(date);
    } catch (e) {
      return dateString;
    }
  }

  void updateRecords(List<Map<String, String>> newRecords) {
    setState(() {
      widget.records.clear();
      widget.records.addAll(newRecords);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(35, 35, 35, 1),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 25,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AppText(
                      text: 'Subscription',
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontSize: 26,
                    ),
                    AppText(
                      text: 'Per month 48\$',
                      color: Colors.white,
                      fontWeight: FontWeight.w400,
                      fontSize: 16,
                    ),
                  ],
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AddNoticeScreen(
                          records: widget.records,
                          updateRecordsCallback: updateRecords,
                        ),
                      ),
                    );
                  },
                  child: SvgPicture.asset("assets/svg/plus_blue.svg"),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const AppText(
                  text: 'Reminder',
                  color: Colors.white,
                  fontWeight: FontWeight.w500,
                  fontSize: 18,
                ),
                const SizedBox(
                  height: 15,
                ),
                const SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      BlueTag1(
                        imgPath: 'assets/img/spotify.png',
                      ),
                      BlueTag2(
                        imgPath: 'assets/img/music.png',
                      ),
                      BlueTag2(
                        imgPath: 'assets/img/spotify.png',
                      ),
                      BlueTag1(
                        imgPath: 'assets/img/music.png',
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  padding: const EdgeInsets.all(3),
                  width: double.maxFinite,
                  height: 40,
                  decoration: BoxDecoration(
                    color: Colors.grey[800],
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            isActiveSelected = true;
                          });
                        },
                        child: Container(
                          height: 43,
                          width: MediaQuery.of(context).size.width * 0.44,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: isActiveSelected
                                ? Colors.white
                                : Colors.grey[800],
                          ),
                          child: Center(
                            child: AppText(
                              text: 'Active',
                              color: isActiveSelected
                                  ? Colors.black
                                  : Colors.white,
                              fontWeight: FontWeight.w600,
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            isActiveSelected = false;
                          });
                        },
                        child: Container(
                          height: 43,
                          width: MediaQuery.of(context).size.width * 0.44,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: !isActiveSelected
                                ? Colors.white
                                : Colors.grey[800],
                          ),
                          child: Center(
                            child: AppText(
                              text: 'Inactive',
                              color: !isActiveSelected
                                  ? Colors.black
                                  : Colors.white,
                              fontWeight: FontWeight.w600,
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                SizedBox(
                  height: MediaQuery.sizeOf(context).height * 0.4,
                  child: SingleChildScrollView(
                    child: Column(
                      children: widget.records.map((record) {
                        return GreyTag(
                          imgPath: record['imagePath'].toString(),
                          desc:
                              'Last paid ${record['cost']}\$ on ${formatDateString(record['subDate']!)}',
                          appName: record['name'] ?? '',
                        );
                      }).toList(),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
