import 'package:flutter/material.dart';
import 'package:notice_app/widgets/list_tile.dart';
import 'package:notice_app/widgets/text_field.dart';
import 'package:notice_app/widgets/text_widget.dart';

class AddNoticeScreen extends StatefulWidget {
  final List<Map<String, String>> records;
  final Function(List<Map<String, String>>) updateRecordsCallback;

  const AddNoticeScreen(
      {super.key, required this.records, required this.updateRecordsCallback});

  @override
  State<AddNoticeScreen> createState() => _AddNoticeScreenState();
}

class _AddNoticeScreenState extends State<AddNoticeScreen>
    with RestorationMixin {
  String selectedImagePath = 'assets/img/add_img.png';
  bool isActiveSelected = true;

  final TextEditingController nameController = TextEditingController();
  final TextEditingController costController = TextEditingController();
  final TextEditingController urlController = TextEditingController();
  final TextEditingController noteController = TextEditingController();
  final TextEditingController subDateController = TextEditingController();
  final TextEditingController expDateController = TextEditingController();

  final RestorableDateTime _selectedSubDate =
      RestorableDateTime(DateTime.now());
  final RestorableDateTime _selectedExpDate =
      RestorableDateTime(DateTime.now());

  late final RestorableRouteFuture<DateTime?>
      _restorableSubDatePickerRouteFuture = RestorableRouteFuture<DateTime?>(
    onComplete: (newSelectedDate) =>
        _selectDate(newSelectedDate, subDateController),
    onPresent: (NavigatorState navigator, Object? arguments) {
      return navigator.restorablePush(
        _datePickerRoute,
        arguments: _selectedSubDate.value.millisecondsSinceEpoch,
      );
    },
  );

  late final RestorableRouteFuture<DateTime?>
      _restorableExpDatePickerRouteFuture = RestorableRouteFuture<DateTime?>(
    onComplete: (newSelectedDate) =>
        _selectDate(newSelectedDate, expDateController),
    onPresent: (NavigatorState navigator, Object? arguments) {
      return navigator.restorablePush(
        _datePickerRoute,
        arguments: _selectedExpDate.value.millisecondsSinceEpoch,
      );
    },
  );

  @override
  String? get restorationId => 'add_notice_screen';

  @pragma('vm:entry-point')
  static Route<DateTime> _datePickerRoute(
    BuildContext context,
    Object? arguments,
  ) {
    return DialogRoute<DateTime>(
      context: context,
      builder: (BuildContext context) {
        return DatePickerDialog(
          restorationId: 'date_picker_dialog',
          initialEntryMode: DatePickerEntryMode.calendarOnly,
          initialDate: DateTime.fromMillisecondsSinceEpoch(arguments! as int),
          firstDate: DateTime(2020),
          lastDate: DateTime(2030),
        );
      },
    );
  }

  void _saveRecord() {
    final newRecord = {
      'name': nameController.text,
      'cost': costController.text,
      'url': urlController.text,
      'note': noteController.text,
      'subDate': subDateController.text,
      'expDate': expDateController.text,
      'imagePath': selectedImagePath,
    };
    widget.records.insert(0, newRecord);
    widget.updateRecordsCallback(List.from(widget.records));
  }

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    registerForRestoration(_selectedSubDate, 'selected_sub_date');
    registerForRestoration(
        _restorableSubDatePickerRouteFuture, 'sub_date_picker_route_future');
    registerForRestoration(_selectedExpDate, 'selected_exp_date');
    registerForRestoration(
        _restorableExpDatePickerRouteFuture, 'exp_date_picker_route_future');
  }

  void _selectDate(
      DateTime? newSelectedDate, TextEditingController controller) {
    if (newSelectedDate != null) {
      setState(() {
        controller.text =
            '${newSelectedDate.day}/${newSelectedDate.month}/${newSelectedDate.year}';
      });
    }
  }

  void _showIconPicker() {
    showModalBottomSheet(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(top: Radius.circular(15.0)),
      ),
      context: context,
      builder: (BuildContext context) {
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          height: MediaQuery.sizeOf(context).height * 0.3,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              AppListTile(
                title: 'Music',
                imgPath: 'assets/img/music.png',
                onSelect: (path) {
                  setState(() {
                    selectedImagePath = path;
                  });
                },
              ),
              AppListTile(
                title: 'Spotify',
                imgPath: 'assets/img/spotify.png',
                onSelect: (path) {
                  setState(() {
                    selectedImagePath = path;
                  });
                },
              ),
              AppListTile(
                title: 'Youtube',
                imgPath: 'assets/img/youtube.png',
                onSelect: (path) {
                  setState(() {
                    selectedImagePath = path;
                  });
                },
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(35, 35, 35, 1),
        // elevation: 0,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios_new_rounded,
          ),
          color: Colors.white,
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      backgroundColor: const Color.fromRGBO(35, 35, 35, 1),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                Center(
                  child: SizedBox(
                    width: MediaQuery.sizeOf(context).width * 0.25,
                    height: MediaQuery.sizeOf(context).width * 0.25,
                    child: CircleAvatar(
                      backgroundColor: const Color.fromRGBO(217, 217, 217, 1),
                      child: GestureDetector(
                        onTap: _showIconPicker,
                        child: Image(
                          image: AssetImage(selectedImagePath),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: MediaQuery.sizeOf(context).width * 0.20,
                  child: AppTextField(
                    label: 'Name',
                    fontSize: 30,
                    fontWeight: FontWeight.w600,
                    controller: nameController,
                    maxLength: 10,
                  ),
                ),
                AppTextField(
                  label: 'Cost',
                  controller: costController,
                  keyboardType: TextInputType.number,
                  maxLength: 5,
                ),
                AppTextField(
                  label: 'Site URL',
                  controller: urlController,
                ),
                AppTextField(
                  label: 'Note',
                  controller: noteController,
                ),
                AppTextField(
                  label: 'Subscription date',
                  controller: subDateController,
                  onTap: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                    _restorableSubDatePickerRouteFuture.present();
                  },
                ),
                AppTextField(
                  label: 'Expiration date',
                  controller: expDateController,
                  onTap: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                    _restorableExpDatePickerRouteFuture.present();
                  },
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 30.0),
              child: SizedBox(
                height: 40,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.blue,
                    minimumSize: const Size(double.infinity, 50),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12),
                    ),
                  ),
                  onPressed: () {
                    _saveRecord();
                    Navigator.pop(context);
                  },
                  child: const AppText(
                    text: 'Send me notifications',
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                    fontSize: 16,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
