import 'package:flutter/material.dart';
import 'package:notice_app/widgets/grey_tag.dart';
import 'package:notice_app/widgets/text_widget.dart';

class CalendarScreen extends StatefulWidget {
  const CalendarScreen({super.key});

  @override
  State<CalendarScreen> createState() => _CalendarScreenState();
}

class _CalendarScreenState extends State<CalendarScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(35, 35, 35, 1),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 25,
            ),
            const AppText(
              text: 'Calender',
              color: Colors.white,
              fontWeight: FontWeight.w500,
              fontSize: 26,
            ),
            const SizedBox(
              height: 15,
            ),
            const SizedBox(
              width: double.maxFinite,
              child: FittedBox(
                fit: BoxFit.fill,
                child: Image(
                  image: AssetImage('assets/img/calen.png'),
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            const AppText(
              text: 'Next payment',
              color: Colors.white,
              fontWeight: FontWeight.w500,
              fontSize: 22,
            ),
            Expanded(
              child: ListView(
                children: const [
                  GreyTag(
                    imgPath: 'assets/img/spotify.png',
                    desc: '9\$ on 19 October,2022',
                    appName: 'Spotify',
                  ),
                  GreyTag(
                    imgPath: 'assets/img/music.png',
                    desc: '9\$ on 8 October,2022',
                    appName: 'Music',
                  ),
                  GreyTag(
                    imgPath: 'assets/img/youtube.png',
                    desc: '9\$ on 8 October,2022',
                    appName: 'Youtube',
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
