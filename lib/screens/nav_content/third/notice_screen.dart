import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:notice_app/widgets/grey_tag2.dart';
import 'package:notice_app/widgets/text_widget.dart';

class NoticeScreen extends StatefulWidget {
  const NoticeScreen({super.key});

  @override
  State<NoticeScreen> createState() => _NoticeScreenState();
}

class _NoticeScreenState extends State<NoticeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(35, 35, 35, 1),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 25,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const AppText(
                  text: 'Notifications',
                  color: Colors.white,
                  fontWeight: FontWeight.w500,
                  fontSize: 26,
                ),
                SizedBox(
                    height: 40,
                    width: 40,
                    child: SvgPicture.asset("assets/svg/plus_blue.svg")),
              ],
            ),
            const SizedBox(
              height: 25,
            ),
            const GreyTag2(
              imgPath: 'assets/img/spotify.png',
              desc: 'Set reminder',
              appName: 'Spotify',
              switchValue: false,
            ),
            const GreyTag2(
              imgPath: 'assets/img/youtube.png',
              desc: '14.05.2024',
              appName: 'Youtube',
              switchValue: true,
            ),
          ],
        ),
      ),
    );
  }
}
