import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:notice_app/widgets/text_widget.dart';

class SettingScreen extends StatefulWidget {
  const SettingScreen({super.key});

  @override
  State<SettingScreen> createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  bool notificationsOn = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(35, 35, 35, 1),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 25,
            ),
            const AppText(
              text: 'Settings',
              color: Colors.white,
              fontWeight: FontWeight.w500,
              fontSize: 26,
            ),
            const SizedBox(
              height: 25,
            ),
            Expanded(
              child: ListView(
                itemExtent: 50,
                children: ListTile.divideTiles(
                  // this adds the dividers
                  context: context,
                  tiles: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const AppText(
                          text: 'Notifications',
                          color: Colors.white,
                          fontSize: 20,
                        ),
                        CupertinoSwitch(
                          value: notificationsOn,
                          activeColor: CupertinoColors.activeBlue,
                          onChanged: (bool? value) {
                            // setState(() {
                            notificationsOn = value ?? false;
                            // });
                          },
                        ),
                      ],
                    ),
                    const AppText(
                      text: 'Support',
                      color: Colors.blue,
                      fontSize: 20,
                    ),
                    const AppText(
                      text: 'Share app',
                      color: Colors.blue,
                      fontSize: 20,
                    ),
                    const AppText(
                      text: 'Rate us',
                      color: Colors.blue,
                      fontSize: 20,
                    ),
                    const AppText(
                      text: 'Delete account',
                      color: Colors.red,
                      fontSize: 20,
                    ),
                  ],
                ).toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
