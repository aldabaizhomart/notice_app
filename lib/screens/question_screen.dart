import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:notice_app/screens/personal_screen.dart';
import 'package:notice_app/widgets/text_widget.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class QuestionScreen extends StatefulWidget {
  const QuestionScreen({super.key});

  @override
  State<QuestionScreen> createState() => _QuestionScreenState();
}

class _QuestionScreenState extends State<QuestionScreen> {
  final PageController _pageController = PageController(initialPage: 0);
  late List<Map<String, dynamic>> _questions;
  Map<String, String?> _selectedOptions = {};

  @override
  void initState() {
    super.initState();
    loadQuestions().then((data) {
      setState(() {
        _questions = data;
        for (var question in _questions) {
          _selectedOptions[question['question']] = null;
        }
      });
    });
  }

  Future<List<Map<String, dynamic>>> loadQuestions() async {
    final String response =
        await rootBundle.loadString('assets/json/questions.json');
    final data = await json.decode(response);
    return List<Map<String, dynamic>>.from(data);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(35, 35, 35, 1),
      body: Padding(
        padding:
            const EdgeInsets.only(left: 15.0, right: 15, top: 50, bottom: 15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SmoothPageIndicator(
              controller: _pageController,
              count: _questions.length,
              effect: WormEffect(
                dotHeight: 10,
                dotWidth: 10,
                activeDotColor: Colors.blue,
                dotColor: Colors.blue.withOpacity(0.3),
              ),
              onDotClicked: (index) => _pageController.animateToPage(
                index,
                duration: const Duration(milliseconds: 200),
                curve: Curves.easeIn,
              ),
            ),
            SizedBox(
              height: 400,
              child: PageView.builder(
                controller: _pageController,
                itemCount: _questions.length,
                itemBuilder: (context, index) {
                  return buildQuestionSlide(_questions[index]);
                },
              ),
            ),
            SizedBox(
              height: 40,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.blue,
                  minimumSize: const Size(double.infinity, 50),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                ),
                onPressed: () {
                  if (_pageController.page!.toInt() == _questions.length - 1) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => PersonificationScreen()));
                  } else {
                    _pageController.nextPage(
                      duration: const Duration(milliseconds: 400),
                      curve: Curves.easeInOut,
                    );
                  }
                },
                child: const AppText(
                  text: 'Continue',
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                  fontSize: 16,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildQuestionSlide(Map<String, dynamic> questionData) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          AppText(
            text: questionData['question'],
            color: Colors.white,
            fontSize: 22,
            fontWeight: FontWeight.w500,
            textAlign: TextAlign.left,
          ),
          Column(
            children: questionData['options'].map<Widget>((option) {
              bool isSelected =
                  _selectedOptions[questionData['question']] == option;
              return Padding(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    foregroundColor: Colors.white,
                    backgroundColor: const Color.fromRGBO(75, 156, 249, 0.5),
                    minimumSize: const Size(double.infinity, 50),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(100),
                      side: BorderSide(
                        color: isSelected
                            ? Colors.white
                            : Colors.white
                                .withOpacity(0), // Adjusted for null safety
                        width: 2,
                      ),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      _selectedOptions[questionData['question']] =
                          option; // Update the selected option
                    });
                  },
                  child: Text(option),
                ),
              );
            }).toList(),
          ),
        ],
      ),
    );
  }
}
