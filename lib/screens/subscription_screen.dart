import 'package:flutter/material.dart';

import 'nav_content/first/list_notice_screen.dart';
import 'nav_content/fourth/setting_screen.dart';
import 'nav_content/second/calendar_screen.dart';
import 'nav_content/third/notice_screen.dart';

class SubscriptionScreen extends StatefulWidget {
  const SubscriptionScreen({super.key});

  @override
  State<SubscriptionScreen> createState() => _SubscriptionScreenState();
}

class _SubscriptionScreenState extends State<SubscriptionScreen> {
  int _selectedIndex = 0;
  List<Map<String, String>> records = [];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  static List<Widget> getScreens(List<Map<String, String>> records) {
    return <Widget>[
      ListNoticeScreen(records: records),
      const CalendarScreen(),
      const NoticeScreen(),
      const SettingScreen(),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: _selectedIndex,
        children: getScreens(records),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        elevation: 0,
        backgroundColor: const Color.fromRGBO(35, 35, 35, 1),
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Image(image: AssetImage('assets/img/home.png')),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Image(image: AssetImage('assets/img/calendar.png')),
            label: 'Calendar',
          ),
          BottomNavigationBarItem(
            icon: Image(image: AssetImage('assets/img/notif.png')),
            label: 'Notifications',
          ),
          BottomNavigationBarItem(
            icon: Image(image: AssetImage('assets/img/setting.png')),
            label: 'Settings',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.blueAccent,
        unselectedItemColor: Colors.blueAccent,
        onTap: _onItemTapped,
      ),
    );
  }
}
