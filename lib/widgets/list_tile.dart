import 'package:flutter/material.dart';

class AppListTile extends StatelessWidget {
  final String title;
  final String imgPath;
  final Function(String) onSelect; // Callback function

  const AppListTile({
    super.key,
    required this.title,
    required this.imgPath,
    required this.onSelect, // Initialize in constructor
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: SizedBox(
        height: 25,
        width: 25,
        child: Image.asset(imgPath),
      ),
      title: Text(title),
      onTap: () {
        onSelect(imgPath); // Call the callback function with the image path
        Navigator.pop(context);
      },
    );
  }
}
