import 'package:capped_progress_indicator/capped_progress_indicator.dart';
import 'package:flutter/material.dart';
import 'package:notice_app/widgets/text_widget.dart';

class BlueTag1 extends StatelessWidget {
  final String imgPath;
  const BlueTag1({super.key, required this.imgPath});

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(right: 10),
        padding: EdgeInsets.symmetric(horizontal: 18, vertical: 12),
        width: MediaQuery.sizeOf(context).width * 0.33,
        height: 100,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: Color.fromRGBO(76, 161, 254, 0.8),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image(
                  image: AssetImage(imgPath),
                ),
                Column(
                  children: [
                    AppText(
                      text: 'Spotify',
                      color: Colors.black,
                      fontWeight: FontWeight.w500,
                      fontSize: 14,
                    ),
                    AppText(
                      text: '6\$',
                      color: Colors.black.withOpacity(0.5),
                      fontWeight: FontWeight.w500,
                      fontSize: 11,
                    ),
                  ],
                )
              ],
            ),
            AppText(
              text: '5 days remaining',
              color: Colors.black,
              fontWeight: FontWeight.w500,
              fontSize: 11,
            ),
            LinearCappedProgressIndicator(
              value: 0.8,
              minHeight: 7,
              color: Color.fromRGBO(51, 53, 143, 1),
            ),
          ],
        ));
  }
}
