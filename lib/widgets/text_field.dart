import 'package:flutter/material.dart';

class AppTextField extends StatelessWidget {
  final String label;
  final double fontSize;
  final FontWeight fontWeight;
  final TextEditingController? controller;
  final VoidCallback? onTap;
  final TextInputType keyboardType;
  final int maxLength;
  const AppTextField(
      {super.key,
      required this.label,
      this.fontSize = 16,
      this.fontWeight = FontWeight.w500,
      this.controller,
      this.onTap,
      this.keyboardType = TextInputType.name,
      this.maxLength = 30});

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      onTap: onTap,
      maxLength: maxLength,
      keyboardType: keyboardType,
      decoration: InputDecoration(
        counterText: '',
        labelText: label,
        labelStyle: TextStyle(
          color: Colors.white,
          fontSize: fontSize,
          fontWeight: fontWeight,
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white54),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.blueAccent),
        ),
      ),
      style: TextStyle(color: Colors.white),
      cursorColor: Colors.blueAccent,
    );
  }
}
