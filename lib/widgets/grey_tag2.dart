import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:notice_app/widgets/text_widget.dart';

class GreyTag2 extends StatelessWidget {
  final String imgPath;
  final String desc;
  final String appName;
  final bool switchValue;
  const GreyTag2(
      {super.key,
      required this.imgPath,
      required this.desc,
      required this.appName,
      required this.switchValue});

  @override
  Widget build(BuildContext context) {
    bool _switchValue = switchValue;

    return Container(
      margin: EdgeInsets.only(top: 10),
      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
      width: double.maxFinite,
      height: 70,
      decoration: BoxDecoration(
        color: Colors.grey[800],
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: 50,
                child: Image(
                  image: AssetImage(imgPath),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AppText(text: appName, fontSize: 18),
                  Row(
                    children: [
                      Icon(
                        Icons.calendar_today,
                        color: Colors.blueAccent,
                        size: 20,
                      ),
                      AppText(
                        text: desc,
                        fontSize: 14,
                        color: Colors.blueAccent,
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
          CupertinoSwitch(
            value: _switchValue,
            activeColor: CupertinoColors.activeBlue,
            onChanged: (bool? value) {
              // setState(() {
              _switchValue = value ?? false;
              // });
            },
          ),
        ],
      ),
    );
  }
}
