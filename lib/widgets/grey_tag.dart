import 'package:flutter/material.dart';
import 'package:notice_app/widgets/text_widget.dart';

class GreyTag extends StatelessWidget {
  final String imgPath;
  final String desc;
  final String appName;
  const GreyTag(
      {super.key,
      required this.imgPath,
      required this.desc,
      required this.appName});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
      width: double.maxFinite,
      height: 70,
      decoration: BoxDecoration(
        color: Colors.grey[800],
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: 50,
                child: Image(
                  image: AssetImage(imgPath),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AppText(text: appName, fontSize: 18),
                  AppText(
                    text: desc,
                    fontSize: 14,
                    color: Colors.blueAccent,
                  ),
                ],
              ),
            ],
          ),
          RotatedBox(
            quarterTurns: 3,
            child: Icon(
              Icons.arrow_back_ios_new_rounded,
              color: Colors.white,
            ),
          )
        ],
      ),
    );
  }
}
